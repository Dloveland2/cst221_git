#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#define MAX 5

void *Producer();
void put();
void *Consumer();
void consume();

int produce();
int get();
int Product;
int buffer[MAX]; 


void *Producer() {
	
	int i;
	
	while(1) {
		
		i = produce();
		
		put(i);
		
		sleep(1);
	}

}

void *Consumer() {

	int i;
	
	while(1) {
		
		i = get();
		
		consume(i);
		
		sleep(1);
	}
}

int produce() {
	
	return Product + 1;
}

void consume(int i) {
	
	printf("Consumed %i\n", i);
	
}

void put(int i) {
	
	Product = i;
	
	printf("Produced %d\n", i);
	
	return;
}

int get() {
	
	return Product;
}

void main() {

	pthread_t threads[2];
	
	pthread_create(&threads[0], 0, Consumer, 0);
	
	pthread_create(&threads[1], 0, Producer, 0);
	
	pthread_join(threads[0], 0);
	
	pthread_join(threads[1], 0);
}

